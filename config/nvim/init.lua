-- {{{ Packages 
local use = require('packer').use

require('packer').startup(function()

    use 'wbthomason/packer.nvim' -- Package manager

    use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}
    use 'williamboman/nvim-lsp-installer'
    use 'ray-x/lsp_signature.nvim'
    use 'neovim/nvim-lspconfig'  -- Configurations for Nvim LSP

    use { 'hrsh7th/nvim-cmp',
         config = function ()
         require'cmp'.setup {
            snippet = {
                expand = function(args)
                require'luasnip'.lsp_expand(args.body)
            end
        },
        sources = { { name = 'luasnip' }, },
        }
    end
    }

    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-path'
    use 'hrsh7th/cmp-cmdline'
    use 'saadparwaiz1/cmp_luasnip'
    use 'nvim-lua/plenary.nvim'
    use 'nvim-telescope/telescope.nvim'

    use {'ray-x/guihua.lua', run = 'cd lua/fzy && make'}

    -- snippet
    use 'rafamadriz/friendly-snippets'
    use 'L3MON4D3/LuaSnip'

    -- status bar
    use { 'nvim-lualine/lualine.nvim', requires = { 'kyazdani42/nvim-web-devicons', opt = true } }

    -- Comment code
    use 'terrortylor/nvim-comment'

    -- Git support
    use 'tpope/vim-fugitive'


    -- File explorer
    use { 'kyazdani42/nvim-tree.lua', requires = { 'kyazdani42/nvim-web-devicons', }, tag = 'nightly' }

    -- Golang 
    -- use 'fatih/vim-go'  
    use 'ray-x/go.nvim'

    -- Themes
    use 'drewtempelmeyer/palenight.vim'

end)

-- }}}

-- {{{ Plugins Configuration

-- {{{ guihua

 -- default mapping
  maps = {
    close_view = '<C-e>',
    save = '<C-s>',
    jump_to_list = '<C-w>k',
    jump_to_preview = '<C-w>j',
    prev = '<C-p>',
    next = '<C-n>',
    pageup = '<C-b>',
    pagedown = '<C-f>',
    confirm = '<C-o>',
    split = '<C-s>',
    vsplit = '<C-v>',
  }

  --
  require('guihua.maps').setup({
  maps = {
    close_view = '<C-x>',
  }
  })
-- }}}

-- {{{ tree-sitter
require'nvim-treesitter.configs'.setup {
  -- A list of parser names, or "all"
  ensure_installed = { "lua", "go", "python",  "bash"},

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- List of parsers to ignore installing (for "all")
  ignore_install = { "javascript" },

  highlight = {
    -- `false` will disable the whole extension
    enable = true,

    -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
    -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
    -- the name of the parser)
    -- list of language that will be disabled
    disable = { "c", "rust" },

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
}
-- }}}

-- {{{ snippet
require("luasnip.loaders.from_vscode").lazy_load({ include = { "go" } })
-- }}}

-- {{{ nvim-tree
require("nvim-tree").setup({
    view = {
        adaptive_size = true,
        update_cwd = true,
        mappings = {
            list = {
                { key = 'l', action = 'open_file' },
                { key = 'h', action = 'close_node' },
            },
        }
    },
})

-- }}}

-- {{{ nvim-cmp
local cmp = require'cmp'

local cmp_kinds = {
  Text = '  ',
  Method = '  ',
  Function = '  ',
  Constructor = '  ',
  Field = '  ',
  Variable = '  ',
  Class = '  ',
  Interface = '  ',
  Module = '  ',
  Property = '  ',
  Unit = '  ',
  Value = '  ',
  Enum = '  ',
  Keyword = '  ',
  Snippet = '  ',
  Color = '  ',
  File = '  ',
  Reference = '  ',
  Folder = '  ',
  EnumMember = '  ',
  Constant = '  ',
  Struct = '  ',
  Event = '  ',
  Operator = '  ',
  TypeParameter = '  ',
}

cmp.setup({
    snippet = {
        expand = function(args)
           require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
        end},

    window = {
      completion = cmp.config.window.bordered(),
      documentation = cmp.config.window.bordered(),
    },

    mapping = cmp.mapping.preset.insert({
      ['<C-Space>'] = cmp.mapping.complete(),
      ['<Tab>'] = cmp.mapping.confirm({ select = true }), 
      ['<CR>'] = cmp.mapping.confirm({ select = true }), 
    }),

    sources = cmp.config.sources({
      { name = 'nvim_lsp' },
      { name = "treesitter" },
      { name = 'buffer' },
      { name = 'luasnip' }, 
      { name = "path" },
    }),

    formatting = {
        fields = { "kind", "abbr", "menu" },
        format = function(entry, vim_item)
          local kind = require("lspkind").cmp_format({ mode = "symbol_text", maxwidth = 50 })(entry, vim_item)
          local strings = vim.split(kind.kind, "%s", { trimempty = true })
          kind.kind = " " .. strings[1] .. " "
          kind.menu = "    (" .. strings[2] .. ")"

          return kind
        end,
  },
})

-- Set configuration for specific filetype.
cmp.setup.filetype('gitcommit', {
  sources = cmp.config.sources({
      { name = 'cmp_git' }, -- You can specify the `cmp_git` source if you were installed it.
    }, {
      { name = 'buffer' }, 
  })
})

-- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline('/', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
      { name = 'buffer' }
    }
})

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
      { name = 'path' }
    }, {
      { name = 'cmdline' }
    })
})

-- }}}

-- {{{ nvim-comment
require('nvim_comment').setup()
-- }}}

-- {{{ ray-x/go
require('go').setup()
-- Run gofmt + goimport on save
vim.api.nvim_exec([[ autocmd BufWritePre *.go :silent! lua require('go.format').goimport() ]], false)

-- }}}

-- {{{ lualine
require('lualine').setup()
-- }}}

-- {{{ lsp_signature

cfg = {
  debug = false, -- set to true to enable debug logging
  log_path = vim.fn.stdpath("cache") .. "/lsp_signature.log", -- log dir when debug is on
  -- default is  ~/.cache/nvim/lsp_signature.log
  verbose = false, -- show debug line number

  bind = true, -- This is mandatory, otherwise border config won't get registered.
               -- If you want to hook lspsaga or other signature handler, pls set to false
  doc_lines = 10, -- will show two lines of comment/doc(if there are more than two lines in doc, will be truncated);
                 -- set to 0 if you DO NOT want any API comments be shown
                 -- This setting only take effect in insert mode, it does not affect signature help in normal
                 -- mode, 10 by default

  floating_window = true, -- show hint in a floating window, set to false for virtual text only mode

  floating_window_above_cur_line = true, -- try to place the floating above the current line when possible Note:
  -- will set to true when fully tested, set to false will use whichever side has more space
  -- this setting will be helpful if you do not want the PUM and floating win overlap

  floating_window_off_x = 1, -- adjust float windows x position.
  floating_window_off_y = 0, -- adjust float windows y position.


  fix_pos = false,  -- set to true, the floating window will not auto-close until finish all parameters
  hint_enable = true, -- virtual hint enable
  hint_prefix = "🐼 ",  -- Panda for parameter
  hint_scheme = "String",
  hi_parameter = "LspSignatureActiveParameter", -- how your parameter will be highlight
  max_height = 12, -- max height of signature floating_window, if content is more than max_height, you can scroll down
                   -- to view the hiding contents
  max_width = 80, -- max_width of signature floating_window, line will be wrapped if exceed max_width
  handler_opts = {
    border = "rounded"   -- double, rounded, single, shadow, none
  },

  always_trigger = false, -- sometime show signature on new line or in middle of parameter can be confusing, set it to false for #58

  auto_close_after = nil, -- autoclose signature float win after x sec, disabled if nil.
  extra_trigger_chars = {}, -- Array of extra characters that will trigger signature completion, e.g., {"(", ","}
  zindex = 200, -- by default it will be on top of all floating windows, set to <= 50 send it to bottom

  padding = '', -- character to pad on left and right of signature can be ' ', or '|'  etc

  transparency = nil, -- disabled by default, allow floating win transparent value 1~100
  shadow_blend = 36, -- if you using shadow as border use this set the opacity
  shadow_guibg = 'Black', -- if you using shadow as border use this set the color e.g. 'Green' or '#121315'
  timer_interval = 200, -- default timer check interval set to lower value if you want to reduce latency
  toggle_key = '<M-x>' -- toggle signature on and off in insert mode,  e.g. toggle_key = '<M-x>'
}

require "lsp_signature".setup(cfg)

-- }}}

--- }}}

-- {{{ Configure LSP

-- Call nvim lsp before setting any server
require("nvim-lsp-installer").setup {}

-- Setup lspconfig.
local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
-- Set each lsp server you've enabled.
require('lspconfig')['gopls'].setup{capabilities = capabilities}
require('lspconfig')['pyright'].setup{capabilities = capabilities}

--- }}}

-- {{{ Key mappings
local bufopts = { noremap=true, silent=true, buffer=bufnr }
vim.g.mapleader = " " 
-- lsp
vim.keymap.set('n', '<leader>f', vim.lsp.buf.formatting, bufopts)
vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
vim.api.nvim_set_keymap('n', 'gu', '<cmd>lua vim.lsp.buf.incoming_calls()<CR>', bufopts)
vim.api.nvim_set_keymap('n', '[g', '<cmd>lua vim.diagnostic.goto_prev()<CR>', bufopts)
vim.api.nvim_set_keymap('n', ']g', '<cmd>lua vim.diagnostic.goto_next()<CR>', bufopts)

-- Telescope
vim.keymap.set('n', '<C-p>', ':Telescope find_files<CR>', bufopts )
vim.keymap.set('n', '<leader>ps', ':Telescope live_grep<CR>', bufopts )
vim.keymap.set('n', '<leader>b', ':Telescope buffers<CR>', bufopts )
vim.keymap.set('n', '<leader>dd', ':Telescope diagnostics<CR>', bufopts )

-- nvim-tree
vim.keymap.set('n', '<F3>', ':NvimTreeFindFileToggle<CR>', bufopts )

-- vim-fugitive

--- }}}

-- {{{ General configuration
vim.o.number = true
vim.o.relativenumber = true
vim.o.foldmethod = "marker"
vim.o.smartcase = true
vim.o.hlsearch = true
vim.o.incsearch = true
vim.o.scrolloff = 10
vim.o.swapfile = true
vim.o.dir = '/tmp'    
vim.o.backup = false
vim.o.writebackup = false
vim.o.undodir = '/home/mickus/.config/nvim/undodir'
vim.o.undofile = true
vim.o.splitright = true
vim.o.updatetime = 300
-- vim.o.signcolumn = "yes"


vim.wo.wrap = false
vim.wo.cursorline = true
vim.wo.colorcolumn = "120"

vim.bo.tabstop = 4
vim.bo.softtabstop = 4
vim.bo.shiftwidth = 4
vim.bo.expandtab = true
vim.bo.smartindent = true

-- }}}

-- {{{ Colorscheme
vim.cmd [[ 
    colorscheme palenight
    set background=dark
    hi Normal guibg=NONE ctermbg=NONE
]]
-- }}}
