"
" |"  \    /"  ||"  \    /"  |
"  \   \  //  /  \   \  //   |      Vitor Mickus
"   \\  \/. ./   /\\  \/.    |      https://gitlab.com/vitorhmr
"    \.    //   |: \.        |
"     \\   /    |.  \    /:  |
"      \__/     |___|\__/|___|
"
"   My vim file configuration.

set exrc                                                 " Load local rc
set noerrorbells                                         " Turn off sounds
set number relativenumber                                " Turn hybrid numbers on
set nowrap                                               " Disable wrap text
set ignorecase                                           " Ignore case when searching
set smartcase
set scrolloff=10                                         " Keep 10 lines below and above the cursor
set hlsearch                                             " Highlight search
" Tabs and space
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent                                          " Auto indentation
" Backups, swap files,...
set noswapfile
set nobackup
set nowritebackup
set undodir=~/.config/nvim/undodir
set undofile                                             " Create undo file in undodir
set splitright                                           " New vertical split in the right
set hidden
set updatetime=300
" Don't pass messages to |ins-completion-menu|.
set shortmess+=c
set signcolumn=yes

" Plugins
call plug#begin(stdpath('data') . '/plugged')

" {{ Beatify }}
Plug 'dracula/vim'                                      " Theme
Plug 'morhetz/gruvbox'
Plug 'vim-airline/vim-airline'                          " Status bar
Plug 'vim-airline/vim-airline-themes'                   " Status bar themes
Plug 'ryanoasis/vim-devicons'                           " Icons
" {{ Comments }}
Plug 'tpope/vim-commentary'                             " For toggle comments

" {{ Git }}
Plug 'tpope/vim-fugitive'                               " For git plugin
Plug 'junegunn/gv.vim'                                  " Better git

" Completion and LSP
Plug 'neoclide/coc.nvim', {'branch': 'release'}         " For autocompletion

" {{Search}}
Plug 'jremmen/vim-ripgrep'                              " Project search
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" {{ Utiltities }}
Plug 'mbbill/undotree'                                  " Kind a local git, to undo stuff
Plug 'tpope/vim-sleuth'                                 " Configure space tab per project

" {{ Language specifics }}
Plug 'fatih/vim-go'                                     " Golang
Plug 'sebdah/vim-delve'                                 " Golang debug

call plug#end()

" Color scheme and background
colorscheme gruvbox
set background=dark
" Transparency
hi Normal guibg=NONE ctermbg=NONE

" Set 80th column background
set colorcolumn=80
" Highlight current line
set cursorline

" Snippet
let g:deoplete#enable_at_startup = 1

if executable('rg')
    let g:rg_command='rg --vimgrep --glob "!tags"'
    let g:rg_derive_root='true'
    let g:rg_root_types=['.git', 'tags']
    set grepprg=rg\ --vimgrep\ --smart-case\ --hidden\ --follow
endif

" Source specific plugins vim files
source $HOME/.config/nvim/plug-config/fzf.vim
source $HOME/.config/nvim/plug-config/coc.vim
" source $HOME/.config/nvim/plug-config/tagbar.vim
source $HOME/.config/nvim/plug-config/coc-snippets.vim
" All mappings/remaps are here
source $HOME/.config/nvim/plug-config/shortcuts.vim

" Test go documentation as floating window
let g:go_doc_popup_window = 1

augroup clean_on_save
    autocmd!
    autocmd BufWritePre * :%s/\s\+$//e                       " Onsave, remove trailing space
augroup END

" Apply cursorLine only for the current window
augroup CursorLine
  au!
  au VimEnter,WinEnter,BufWinEnter * setlocal cursorline
  au WinLeave * setlocal nocursorline
augroup END
