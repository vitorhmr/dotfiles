" Remap key leader to SPACE
let mapleader = " "

" <Ctrl-l> redraws the screen and removes any search highlighting
nnoremap <silent> <C-l> :nohl<CR><C-l>

" Remaps for window movement
nnoremap <silent> <Leader>+ :vertical resize +5<CR>
nnoremap <silent> <Leader>- :vertical resize -5<CR>

" Undotree
nnoremap <leader>u :UndotreeShow<CR>
" Coc Explorer
nnoremap <F3> :CocCommand explorer --preset floating<CR>
nmap <F2> <Plug>(coc-rename)
" Search files
nnoremap <C-p> :Files<CR>
" Search text in files
nnoremap <leader>ps :RG <CR>
nnoremap <leader>t :Tags<CR>
nnoremap <leader>m :Marks<CR>
nnoremap <leader>b :Buffers<CR>

" Complete parentheis, brackets, curly, "
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O

" Select all
nnoremap <leader>a ggVG
