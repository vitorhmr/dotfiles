#!/bin/bash

CONFIGURATION_PROFILE_NAME="client.ovpn"

vpn_connect() {
    echo "Connecting to ifood vpn"
    openvpn3 session-start --config ${CONFIGURATION_PROFILE_NAME}
}

vpn_disconnect() {
    echo "Disconnecting from ifood vpn"
    openvpn3 session-manage --config ${CONFIGURATION_PROFILE_NAME} --disconnect
}

vpn_restart() {
    echo "Restarting to ifood vpn"
    openvpn3 session-manage --config ${CONFIGURATION_PROFILE_NAME} --restart
}


if [ -n "$1" ]
then

    case "$1" in
        -c)
            vpn_connect
            ;;
        -d)
            vpn_disconnect
            ;;
        -r)
            vpn_restart
            ;;
        *)
            echo "$1 is not a valid option. Valid options: -c, -d, or -r.)"
            ;;
    esac

else
    echo "Specify parameters: "
    echo "    -c: connect to vpn"
    echo "    -d: disconnect vpn"
    echo "    -r: restart vpn"
fi

