#!/bin/bash

# Enter mamute project
cd /home/vm/Projects/devito/
wait

SESSION_NAME='DEVITO'

# Start tmux session
## code window [1]
tmux -f /home/vm/.config/tmux/tmux.conf new-session -d -s ${SESSION_NAME}
tmux rename-window 'code'
tmux move-window -t 1
tmux send-keys -t ${SESSION_NAME}:1 'cd code; vim' C-m

## run window [2]
tmux new-window -t ${SESSION_NAME}
tmux rename-window 'tests'
tmux move-window -t 2
tmux send-keys -t ${SESSION_NAME}:2 'cd tests/wave-propagation-02; vim' C-m

tmux select-window -t ${SESSION_NAME}:1
tmux attach-session -t ${SESSION_NAME}
