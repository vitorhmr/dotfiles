#!/bin/bash

# Enter mamute project
cd /home/vm/Projects/clinicos
wait

SESSION_NAME='CLINICOS'

# Start tmux session
## code window [1]
tmux -f /home/vm/.config/tmux/tmux.conf new-session -d -s ${SESSION_NAME}
tmux rename-window 'code'
tmux move-window -t 1
tmux send-keys -t ${SESSION_NAME}:1 'source env.sh' C-m
tmux send-keys -t ${SESSION_NAME}:1 'cd code; vim' C-m

## run window [2]
tmux new-window -t ${SESSION_NAME}
tmux rename-window 'run-server'
tmux move-window -t 2
tmux send-keys -t ${SESSION_NAME}:2 'source env.sh' C-m
tmux send-keys -t ${SESSION_NAME}:2 'cd code' C-m
# tmux send-keys -t ${SESSION_NAME}:2 'python manage.py runserver 192.168.1.114:8000' C-m
tmux send-keys -t ${SESSION_NAME}:2 'python manage.py runserver' C-m

## run window [3]
tmux new-window -t ${SESSION_NAME}
tmux rename-window 'local db'
tmux move-window -t 3
tmux send-keys -t ${SESSION_NAME}:3 'psql -d clinicos-dev' C-m

tmux select-window -t ${SESSION_NAME}:1
tmux attach-session -t ${SESSION_NAME}

