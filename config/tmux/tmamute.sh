#!/bin/bash

# Enter mamute project
cd /home/vm/Projects/mamute
wait

SESSION_NAME='MAMUTE'

# Start tmux session
## code window [1]
tmux -f /home/vm/.config/tmux/tmux.conf new-session -d -s ${SESSION_NAME}
tmux rename-window 'code'
tmux move-window -t 1
tmux send-keys -t ${SESSION_NAME}:1 'cd code; vim' C-m

## run window [2]
tmux new-window -t ${SESSION_NAME}
tmux rename-window 'utils'
tmux move-window -t 2
tmux send-keys -t ${SESSION_NAME}:2 'cd mamute-utils' C-m

## run window [3]
tmux new-window -t ${SESSION_NAME}
tmux rename-window 'npad'
tmux move-window -t 3
tmux send-keys -t ${SESSION_NAME}:3 'ssh npad' C-m

tmux select-window -t ${SESSION_NAME}:1
tmux attach-session -t ${SESSION_NAME}
