# dotfiles

Hold my most used dot files for linux apps configuration.

## Neovim


### Location

Neovim configuration file at the repository: `.config/nvim/init.lua`

should be placed in your `$HOME/.config/nvim/init.lua`


### Main compomenents

- Install [neovim](https://github.com/neovim/neovim/wiki/Installing-Neovim)
- Install [Packer](https://github.com/wbthomason/packer.nvim#quickstart)


### Binaries that you should have
- fd an alternative for find [fd](https://github.com/sharkdp/fd#installation)
- rg for text searching [ripgrep](https://github.com/BurntSushi/ripgrep)
- fzf for fuzzy finder [sudo apt-get install fzf]
- node and npm for Language Server Protocols  


### After installing
In the neovim run:
```
:PackerInstall
```


## Zsh


## tmux


## i3


## polybar


